from django.shortcuts import render, get_object_or_404, redirect
from alphadogsapp.models import Dogs
from alphadogsapp.forms import DogsForm

def show_all_dogs(request):
    dog_object = Dogs.objects.all()
    context = {
        "show_all_dogs": dog_object,
    }
    return render(request, "webpages/doggos.html", context)

def show_main(request, id):
    dogs = get_object_or_404(Dogs, id=id)
    context = {
        "dog_object": dogs
    }
    return render(request, "webpages/individualdogs.html", context)

def add_dog(request):
    if request.method == "POST":
        form = DogsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("main_page")
    else:
        form = DogsForm()
    context={
        "post_form": form,
    }
    return render(request, "webpages/create.html", context)

def edit_dog(request, id):
    post = get_object_or_404(Dogs, id=id)
    if request.method == "POST":
        form = DogsForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_dog", id=id)
    else:
        form=DogsForm(instance=post)
    context ={
        "post_object": post,
        "post_form": form,
        }
    return render(request, "webpages/edit.html", context)

from django.urls import path
from alphadogsapp.views import show_main, show_all_dogs, add_dog, edit_dog

urlpatterns = [
    path("dogs/<int:id>/", show_main, name="show_dog"),
    path("dogs/", show_all_dogs, name="main_page"),
    path("add_dog", add_dog, name="add_dog"),
    path("dogs/<int:id>/edit/", edit_dog, name="edit_dog")


]

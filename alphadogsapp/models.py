from django.db import models

class Dogs(models.Model):
    name = models.CharField(max_length=100)
    picture = models.URLField()
    description = models.CharField(max_length=200)
    behavior = models.TextField()
    boardingdate = models.DateField()

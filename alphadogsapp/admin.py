from django.contrib import admin
from alphadogsapp.models import Dogs


@admin.register(Dogs)
class DogAdmin(admin.ModelAdmin):
    list_display=(
        "name",
        "id",
        "picture",
        "description",
        "behavior",
        "boardingdate"


    )

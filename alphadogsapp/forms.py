from alphadogsapp.models import Dogs
from django.forms import ModelForm

class DogsForm(ModelForm):
    class Meta:
        model = Dogs
        fields= [
            "name",
            "picture",
            "description",
            "behavior",
            "boardingdate"
        ]
